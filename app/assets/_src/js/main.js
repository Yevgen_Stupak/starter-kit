import detectBrowser from './utils/detectBrowser';
import isMobile from './utils/isMobile';
import removeHoverOnMobile from './utils/removeHoverOnMobile';


class Modules {
  constructor() {
    this.modules = [];
    this.env = 'dev';
  }

  log(str) {
    if (this.env === 'dev') {
      console.log(str);
    }
  }

  addModules(module) {
    this.modules.push(module);
  }

  init() {

    this.modules.forEach((item => {
      if (typeof item.init !== 'undefined') {
        this.log(`Module ${item.constructor.name} is init`);
        item.init();
      }
    }))
  }
}


$(document).ready(() => {
  detectBrowser();
  removeHoverOnMobile();
  const modules = new Modules();
  modules.addModules(new removeHoverOnMobile());

  modules.init();

  if (isMobile()) {
    $('body').addClass('mobile');
  }
});
